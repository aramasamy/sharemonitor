from sklearn import linear_model, datasets
import matplotlib.pyplot as plt
import pandas_datareader.data as web


def perform_linear_regression(start_date, end_date, stock_name, idx_name = '^NDX'):
    #dates = pd.date_range(start_date, end_date)
    df1 = {}
    df1[stock_name] = web.get_data_yahoo(stock_name, start_date, end_date)[['Adj Close']]
    df1[idx_name] = web.get_data_yahoo(idx_name, start_date, end_date)[['Adj Close']]

    # Split the data into training/testing sets
    df1_X = df1[idx_name]
    df1_X_train = df1_X[:-20]
    df1_X_test = df1_X[-20:]

    # Split the data into training/testing sets
    df1_y_train = df1[stock_name][:-20]
    df1_y_test = df1[stock_name][-20:]

    regr = linear_model.LinearRegression()
    regr.fit(df1_X_train.values, df1_y_train.values)
    plt.scatter(df1_X_test.values, df1_y_test.values, color='blue')
    plt.plot(df1_X_test.values, regr.predict(df1_X_test.values), color='blue', linewidth=3)
    plt.show()