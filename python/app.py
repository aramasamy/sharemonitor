#!/usr/bin/env python
# encoding: utf-8

from logging import DEBUG
from datetime import datetime, date
import pandas as pd
import tracker
from functools import partial

from sklearn import svm
from sklearn.covariance import EllipticEnvelope
import numpy as np

from flask import Flask, render_template, request, redirect, url_for, flash, make_response
app = Flask(__name__)
app.logger.setLevel(DEBUG)

def fancy_pancy(row, stock_name, index_name='^NDX', column_name='Adj Close'):
    column_desc_dict = {'Adj Close': 'adjusted closing price'}
    column_desc = column_desc_dict.get(column_name, column_name)

    index_name_dict = {'^NDX': 'NASDAQ100'}
    index_desc = index_name_dict.get(index_name, index_name)

    pct_deviation = row['pct_deviation']*100
    dt = row['Date'].strftime('%d %b')
    if pct_deviation > 2:
        str_message = 'On {date}, {} had a huge deviation of {pct_deviation:.2f}%' \
               ' in {column_desc} compared to {index_desc}.' \
               .format(stock_name, index_desc=index_desc, column_desc=column_desc, 
                       date=dt, pct_deviation=pct_deviation)
    else:
        str_message = 'Unusual activity detected on {date}.'.format(date=dt)
    
    idx_pct = row['pct_adiff']*100
    str_message += ' {index_desc} went {updown} by {idx_pct:.2f}%,'. \
            format(index_desc = index_desc,
                   updown = 'up' if idx_pct > 0 else 'down',
                   idx_pct = idx_pct)

    stock_pct = row['pct_diff']*100
    str_message += ' {stock_name} went {updown} by {stock_pct:.2f}%.'. \
            format(stock_name = stock_name,
                   updown = 'up' if stock_pct > 0 else 'down',
                   stock_pct = stock_pct)

    return pd.Series({'Date': row['Date'], 'message': str_message})

@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html")

@app.route("/portfolio", methods=('GET', 'POST'))
def portfolio():
    if request.method == 'GET':
        return render_template("portfolio.html", user_name="")
    email=request.form['email']
    user_name=email[:email.find('@')]
    user_portfolio={'JPM':[500, 52.19, 63.99, 'JPMorgan Chase & Co.', 26095, 31995, 5900], 'IBM':[20, 162.13, 158.99, 'IBM Limited', 20*162.13, 20*158.99, (-20*162.13+20*158.99)],
               'AAPL':[100, 98.99, 98.78, 'Apple Inc', 100*98.99, 100*98.78, (100*98.78-100*98.99)],
               'GOOG':[50, 720.12, 719.85, 'Alphabet Inc', 50*720.12, 50*719.85, (50*719.85 - 50*720.12)], 'YHOO':[120, 37.12, 38.72, 'Yahoo! Inc', 120*37.12, 120*38.72, (120*38.72 - 120*37.12)]}
    total= 31995 + (20*158.99) + (100*98.78) + (50*719.85) + (120*38.72)
    weight_jpm = int((31995*100/total))
    weight_ibm = int(20*158.99*100/total)
    weight_aapl = int((100*98.78*100)/total)
    weight_goog= int((50*719.85*100)/total)
    weight_yhoo = int((120*38.72*100)/total)
    data = {'labels': user_portfolio.keys(),'backgroundColor':['#F7464A', '#46BFBD', '#FDB45C', '#FEDCBA', '#ABCDEF', '#DDDDDD', '#FFFFF'],
            'weight':[weight_jpm, weight_ibm, weight_aapl, weight_goog, weight_yhoo]}
    return render_template("portfolio.html", user_name=user_name, user_portfolio=user_portfolio, data=data)

@app.route("/quote")
def quote(min_deviation=0.03, str_start_date='2016-01-01', str_end_date='2016-06-30'):
    stock_name=request.args.get('imnt')
    str_start_date=request.args.get('str_start_date') or str_start_date
    str_end_date=request.args.get('str_end_date') or str_end_date

    return render_template("quote.html", stock_name = stock_name, str_start_date = str_start_date, str_end_date = str_end_date)

@app.route("/quote_csv")
def quote_csv(min_deviation=0.02, str_start_date='2016-01-01', str_end_date='2016-06-30'):
    stock_name=request.args.get('imnt')
    str_start_date=request.args.get('str_start_date') or str_start_date
    str_end_date=request.args.get('str_end_date') or str_end_date
    start_date = datetime.strptime(str_start_date[:10], '%Y-%m-%d')
    end_date = datetime.strptime(str_end_date[:10], '%Y-%m-%d')

    result_df = tracker.prepare_data(stock_name, start_date = start_date, end_date = end_date)

    output = make_response(result_df.to_csv())
    output.headers["Content-type"] = "text/csv"
    return output

@app.route("/quote_with_outlier_csv")
def quote_with_outlier_csv(min_deviation=0.02, str_start_date='2016-01-01', str_end_date='2016-06-30'):
    stock_name=request.args.get('imnt')
    str_start_date=request.args.get('str_start_date') or str_start_date
    str_end_date=request.args.get('str_end_date') or str_end_date
    start_date = datetime.strptime(str_start_date[:10], '%Y-%m-%d')
    end_date = datetime.strptime(str_end_date[:10], '%Y-%m-%d')

    learn_df = tracker.prepare_data(stock_name, start_date = date(2015, 1, 1), end_date = date(2016, 1, 1))
    learn_data = learn_df.reset_index()[['pct_diff','pct_adiff']].values
    clf1= EllipticEnvelope(contamination=0.02)
    clf1.fit(learn_data)

    result_df = tracker.prepare_data(stock_name, start_date = start_date, end_date = end_date)
    test_data = result_df[['pct_diff', 'pct_adiff']].values
    prediction = clf1.predict(test_data)
    result_df['prediction'] = -1
    result_df = result_df.transpose()
    result_df.loc['prediction'] = prediction
    result_df = result_df.transpose()
    outlier_df = result_df[result_df['prediction'] < 0]

    fancy_pancy_applied = partial(fancy_pancy, stock_name = stock_name)
    outlier_messages = pd.DataFrame(outlier_df.reset_index().apply(fancy_pancy_applied, axis=1)).set_index('Date')

    result_df = result_df.merge(outlier_messages, how='left', left_index=True, right_index=True)

    output = make_response(result_df.to_csv())
    output.headers["Content-type"] = "text/csv"
    return output

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

if __name__ == "__main__":
    app.run(debug=True)
