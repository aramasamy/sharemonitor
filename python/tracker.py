#!/usr/bin/env python
# encoding: utf-8

import pandas.io.data as web
import datetime
from datetime import date

def prepare_data(stock_name, start_date=date(2016, 1, 1), end_date=date(2016, 6, 30), idx_name = '^NDX', column_to_use = 'Adj Close'):
    """ The method identifies if the stock was an outlier for the given date range """
    dfs = {}
    dfs[stock_name]=web.DataReader(stock_name, 'yahoo', start_date, end_date)
    dfs[idx_name]=web.DataReader(idx_name, 'yahoo', start_date, end_date)

    combined_df = dfs[stock_name].merge(dfs[idx_name], left_index=True, right_index=True, suffixes=['_' + stock_name, '_' + idx_name])
    column_name = column_to_use

    needed_columns = [c + '_' + stock_name for c in 'Open', 'Close', 'High', 'Low', 'Volume']
    needed_columns = needed_columns + [column_name + '_' + stock_name, column_name + '_' + idx_name]

    result_df = combined_df[needed_columns]
    result_df = result_df.reset_index().reset_index()
    result_df['pindex'] = result_df['index'] - 1

    result_df = result_df.merge(result_df, left_on=['pindex'], right_on = ['index'], suffixes = ['', '_prev'])
    result_df = result_df.drop(['index', 'pindex', 'index_prev', 'pindex_prev'], axis=1)

    result_df['diff'] =  result_df[column_name + '_' + stock_name] - result_df[column_name + '_' + stock_name + '_prev']
    result_df['adiff'] =  result_df[column_name + '_' + idx_name] - result_df[column_name + '_' + idx_name + '_prev']
    result_df['pct_diff'] = result_df['diff'] / result_df[column_name + '_' + stock_name + '_prev']
    result_df['pct_adiff'] = result_df['adiff'] / result_df[column_name + '_' + idx_name + '_prev']

    result_df['pct_deviation'] = result_df['pct_diff'] - result_df['pct_adiff']
    result_df['pct_deviation'] = result_df['pct_deviation'].abs()

    result_df = result_df.set_index('Date')

    return result_df

