var google; // allow 
var document;

function drawChart(csvData) {
    function drawVisualization() {
      var data = new google.visualization.DataTable();
      data.addColumn('date', 'Date');
      data.addColumn('number', 'Low');
      data.addColumn('number', 'Open');
      data.addColumn('number', 'Close');
      data.addColumn('number', 'High');
      data.addColumn('number', 'NDX');
      data.addColumn({type:'string', role:'annotation'});
      data.addColumn({type:'string', role:'annotationText', p: {html:false} });
      data.addRows(csvData);

      var options = {
        title : 'Daily price',
        //vAxis: {title: 'Cups'},
        //hAxis: {title: 'Month'},
        isStacked: true,
        legend: 'none',
        'chartArea': {'width': '90%', 'height': '80%'},
        annotations: {
            boxStyle: {
                  // Color of the box outline.
                  stroke: '#FF0000',
                  // Thickness of the box outline.
                  strokeWidth: 4,
                  // x-radius of the corner curvature.
                  rx: 3,
                  // y-radius of the corner curvature.
                  ry: 3},
            textStyle: {
                  fontName: 'Times-Roman',
                  fontSize: 18,
                  bold: true,
                  italic: true,
                  // The color of the text.
                  color: '#871b47',
                  // The color of the text outline.
                  auraColor: '#d799ae',
                  // The transparency of the text.
                  opacity: 0.8
                }
        }, 
        tooltip: {isHtml: true},
        series: {0:{type: "candlesticks", color: 'blue', opacity: 0.3, targetAxisIndex: 0}, 
                 1:{type:"line", color: 'green', targetAxisIndex: 1} }
      };

      var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);
}
